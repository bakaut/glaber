  variables:
    # used for build docker images d
    BUILDER_VERSION: 1.0.1
    # used for build debian based apt
    DEBIAN_BUILDER_VERSION: 1.0.1
    # used for build ubuntu based apt
    UBUNTU_BUILDER_VERSION: 1.0.1
    # used for build centos based yum
    CENTOS_BUILDER_VERSION: 1.0.1
    # used for glager php-fpm7
    ALPINE_PHP_FPM_VERSION: 1.0.1
    KANIKO_VERSION: 1.9.2
  
  stages:
    - pre-build
    - pkg-build
    - docker-build
  
  .kaniko_build: &kaniko_build
    - sh pre-build/scripts/kaniko-build.sh
  
  .add_ssh_key: &add_ssh_key
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY_BASE64}" | base64 -d | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo 'Host *' > ~/.ssh/config
    - echo '  StrictHostKeyChecking no' >> ~/.ssh/config
    - echo '  UserKnownHostsFile /dev/null' >> ~/.ssh/config
    - chmod 400 ~/.ssh/config
  
  .build_docker: &build_docker
    # github api have some api rate limits for non auth users, so,
    # we need to create PACKER_GITHUB_API_TOKEN secret in gitlab
    # https://developer.hashicorp.com/packer/docs/configure#packer_github_api_token
    # https://github.com/settings/tokens?type=beta (Public Repositories (read-only))
    # this code should work without PACKER_GITHUB_API_TOKEN,
    # but if packer init command failed, you must to set this vaiable in repo secrets
    # - export PACKER_GITHUB_API_TOKEN=${PACKER_GITHUB_API_TOKEN}
    - cd build/appliance/build/docker
    - packer init .
    - packer validate .
    - export DOCKER_PASSWORD=${CI_REGISTRY_PASSWORD}
    - packer build -color=false -timestamp-ui -warn-on-undeclared-var .
  
  
  .upload_mysql_schema: &upload_mysql_schema
    - wget -q -O /usr/local/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
    - chmod +x /usr/local/bin/jq
    - cat database/mysql/schema.sql > create.sql
    - cat database/mysql/images.sql >> create.sql
    - cat database/mysql/data.sql >> create.sql
    - tar -czvf create-mysql.sql.tar.gz create.sql
    - curl -s "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages?per_page=1000" | jq --arg name "mysql" '.[] | select(.name == $name)' | grep $GLABER_VERSION || curl -s --header "JOB-TOKEN:$CI_JOB_TOKEN" --upload-file "create-mysql.sql.tar.gz" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/mysql/$GLABER_VERSION/create-mysql.sql.tar.gz"
  
  .build_glbmap: &build_glbmap
    - git clone https://gitlab.com/mikler/glbmap.git
    - cd glbmap && cmake . && make -j4
    - mv src/zmap src/glbmap && chmod +s src/glbmap
    - pwd
    - ls -al
    - cd ../..
    - ls -al
  
  .set_glb_version: &set_glb_version
    - export GLABER_VERSION=$(cat include/version.h | grep GLABER_VERSION | tr -dc 0-9.)
    - echo $GLABER_VERSION
  
  .clean_old_dists: &clean_old_dists
    - rm $FULL_PROJECT_PATH/../glaber*.deb || true
  
  .prepare_sources: &prepare_sources
    - *clean_old_dists
    - *set_glb_version
    - ./bootstrap.sh
    - ./configure
    - make dbschema gettext
    - *upload_mysql_schema
    - autoreconf -fvi
    - cp -r build/${OS}/${OS_VER}/ debian && cd debian
    - sed -i "1 s/(.*+/(1:$GLABER_VERSION-${CI_COMMIT_SHORT_SHA}+/g" changelog
    - head -n 5 changelog
  
  .prepare_rpm_sources: &prepare_rpm_sources
    - *set_glb_version
    - ./bootstrap.sh
    - ./configure
    - make dbschema gettext
    - *upload_mysql_schema
    - autoreconf -fvi
    - cd ..
    - cp -r glaber glaber-$GLABER_VERSION
    - cd glaber-$GLABER_VERSION
    - pwd
    - ls -al
    - rm -rf glbmap
    - git clone https://gitlab.com/mikler/glbmap.git
    - cd glbmap && cmake . && make -j4
    - mv src/zmap ../zmap &&  cd ..
    - rm -rf glbmap
    - mv zmap glbmap && chmod +s ./glbmap && cd ..
    - tar -czvf glaber-$GLABER_VERSION.tar.gz glaber-$GLABER_VERSION
    - cd ${CI_PROJECT_DIR}
    - cp -r build/${OS}/${OS_VER}/* build/centos
    - mv ../glaber-$GLABER_VERSION.tar.gz build/centos/SOURCES/glaber-$GLABER_VERSION.tar.gz
    - sed -i "2 s/Version:.*/Version:\t$GLABER_VERSION/g" build/centos/SPECS/zabbix.spec
  
  .upload_packages: &upload_packages
    - ls
  
  .upload_rpms: &upload_rpms
    - ls
  
  debian-buster:
    stage: pkg-build
    variables:
      OS: debian
      OS_VER: buster
    image: ${CI_REGISTRY_IMAGE}/${OS}-${OS_VER}-builder:${DEBIAN_BUILDER_VERSION}
    script:
      - export FULL_PROJECT_PATH=$PWD
      - *prepare_sources
      - *build_glbmap
      - dpkg-buildpackage -b --no-sign
      - *upload_packages
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  # astra:
  #  stage: pkg-build
  #  variables:
  #    OS: astra
  #    OS_VER: smolensk
  #  tags: ["virtbox"]
  #  script:
  #    - update-ca-certificates
  #    - *build_go
  #    - *prepare_sources
  #    - *build_glbmap
  #    - dpkg-buildpackage -b --no-sign
  #    - *upload_packages
  #  rules:
  #    - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  debian-bullseye:
    stage: pkg-build
    variables:
      OS: debian
      OS_VER: bullseye
    image: ${CI_REGISTRY_IMAGE}/${OS}-${OS_VER}-builder:${DEBIAN_BUILDER_VERSION}
    script:
      - export FULL_PROJECT_PATH=$PWD
      - *prepare_sources
      - *build_glbmap
      - dpkg-buildpackage -b --no-sign
      - *upload_packages
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  ubuntu-focal:
    stage: pkg-build
    variables:
      OS: ubuntu
      OS_VER: focal
    image: ${CI_REGISTRY_IMAGE}/${OS}-${OS_VER}-builder:${UBUNTU_BUILDER_VERSION}
    script:
      - export FULL_PROJECT_PATH=$PWD
      - *prepare_sources
      - *build_glbmap
      - dpkg-buildpackage -b --no-sign
      - *upload_packages
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  ubuntu-bionic:
    stage: pkg-build
    variables:
      OS: ubuntu
      OS_VER: bionic
    image: ${CI_REGISTRY_IMAGE}/${OS}-${OS_VER}-builder:${UBUNTU_BUILDER_VERSION}
    script:
      - export FULL_PROJECT_PATH=$PWD
      - *prepare_sources
      - *build_glbmap
      - dpkg-buildpackage -b --no-sign
      - *upload_packages
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  centos-8:
    stage: pkg-build
    variables:
      OS: almalinux
      OS_VER: 8
    image: ${CI_REGISTRY_IMAGE}/${OS}-${OS_VER}-builder:${CENTOS_BUILDER_VERSION}
    script:
      - export FULL_PROJECT_PATH=$PWD
      - *prepare_rpm_sources
      - cd build/centos
      - yum-builddep -y SPECS/zabbix.spec
      - rpmbuild -bb SPECS/zabbix.spec --define "_sourcedir $PWD/SOURCES" --define "glaber_version $GLABER_VERSION"
      - *upload_rpms
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  docker-debian-bullseye:
    stage: docker-build
    image: ${CI_REGISTRY_IMAGE}/builder:${BUILDER_VERSION}
    before_script:
      - *add_ssh_key
      - export DOCKER_HOST=ssh://${SSH_USER}@${SERVER}
    script:
      - *set_glb_version
      - *build_docker
      - docker rmi -f $(docker images ${CI_REGISTRY_IMAGE}/glaber-server -q)
      - docker rmi -f $(docker images ${CI_REGISTRY_IMAGE}/glaber-nginx -q)
    rules:
      - if: $CI_COMMIT_MESSAGE !~ /pre-build/
  
  builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: alpine
      OS_VER: "3.17"
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}/docker-cli"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: "${BUILDER_VERSION}"
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  debian-buster-builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: debian
      OS_VER: buster
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${DEBIAN_BUILDER_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  debian-bullseye-builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: debian
      OS_VER: bullseye
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${DEBIAN_BUILDER_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  ubuntu-bionic-builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: ubuntu
      OS_VER: bionic
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${UBUNTU_BUILDER_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  ubuntu-focal-builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: ubuntu
      OS_VER: focal
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${UBUNTU_BUILDER_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  almalinux-8-builder:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: almalinux
      OS_VER: 8
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${CENTOS_BUILDER_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
  
  alpine-php-fpm-7:
    stage: pre-build
    image:
      name: gcr.io/kaniko-project/executor:v${KANIKO_VERSION}-debug
      entrypoint: [""]
    variables:
      OS: alpine
      OS_VER: "3.15"
      BUILD_DIR: "${CI_PROJECT_DIR}/pre-build/${OS}/php-fpm-7"
      BUILD_IMG: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}"
      BUILD_TAG: ${ALPINE_PHP_FPM_VERSION}
      CACHE_REPO: "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME}-cache"
    script:
      - *kaniko_build
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /pre-build/
